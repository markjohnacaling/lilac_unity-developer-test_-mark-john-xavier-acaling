using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class Problem_State_1 : MonoBehaviour
{   
    public Text label_numTiles;
    public TMP_InputField rowX_Input;
    public TMP_InputField colY_Input;
    public Transform tile_container;
    public int num_of_tiles;
    public GameObject square_;
    public int rowX = 0;
    public int colY = 0;

    public static Problem_State_1 pb;
    // Start is called before the first frame update
    void Start()
    {
        pb = this;
        rowX_Input.text = rowX.ToString();
        colY_Input.text = colY.ToString();
        generateData();

        
    }

    // Update is called once per frame
    void Update()
    {
       
         //num_of_tiles = colX+colY;
    }
    public void generateData()
    {
        foreach(Transform t in tile_container)
        {
            Destroy(t.gameObject);
        }
        int tile_num = 1;
        rowX = int.Parse(rowX_Input.text.ToString());
        colY = int.Parse(colY_Input.text.ToString());
         for(int y = 0 ; y > (colY * -1) ; y--)
        {
          for(int x = 0; x < rowX; x++)
            {
            GameObject tile_ =  Instantiate(square_, new Vector2((x * 54) +tile_container.position.x ,(y *54) +tile_container.position.y) ,Quaternion.identity, tile_container);
            tile_.gameObject.name = tile_num.ToString() + "_tileNumber";
            tile_script tile_script_ = tile_.GetComponent<tile_script>();
            tile_script_.tile_number = tile_num;
            tile_script_.tile_Y = y;
            tile_script_.tile_X = x;
            tile_num++;
            }
        }
        tile_num--;
        num_of_tiles = tile_num;
        label_numTiles.text = num_of_tiles.ToString();
    }
}
