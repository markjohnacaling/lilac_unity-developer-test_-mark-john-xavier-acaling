using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class tile_script : MonoBehaviour
{
    public Transform grid_container;
    public bool opened;
    public int tile_number;
    public int tile_Y;
    public int tile_X;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void returnTile_number(bool checkTile)
    {
        if(!opened)
        {
        transform.GetChild(1).GetComponent<Text>().text = tile_number.ToString();
        transform.GetChild(0).GetComponent<Image>().color =  new Color32((byte)Random.Range(0,256),(byte)Random.Range(0,256),(byte)Random.Range(0,256),250);
        opened = true;
        GetComponent<Button>().interactable = false;
        checkTile_slots(checkTile);
        }

    }
    public void checkTile_slots(bool checkTile)
    {
        if(!checkTile)
            return;
      
 
    bool leftHorizontal = (tile_number - 1 ) > 0;
    bool rightHorizontal = (tile_number + 1 ) < Problem_State_1.pb.num_of_tiles;
    bool topVertical = (tile_Y + 1) <= 0; 
    bool downVertical = (tile_Y - 1) >= Problem_State_1.pb.colY * -1; 
    if(leftHorizontal)
    {
     
        foreach(Transform t in GameObject.Find("/grid_Container/tile_Container").transform)
        {
            tile_script tile_script_ = t.GetComponent<tile_script>();
            if(tile_script_.tile_number ==  (tile_number - 1 ) && tile_Y == tile_script_.tile_Y)
            {
                tile_script_.returnTile_number(false);
            }
        }

       
    }
    if(rightHorizontal)
    {
     
        foreach(Transform t in GameObject.Find("/grid_Container/tile_Container").transform)
        {
            tile_script tile_script_ = t.GetComponent<tile_script>();
            if(tile_script_.tile_number ==  (tile_number + 1 ) && tile_Y == tile_script_.tile_Y)
            {
                tile_script_.returnTile_number(false);
            }
        }
    }
    if(topVertical)
    {
        foreach(Transform t in GameObject.Find("/grid_Container/tile_Container").transform)
        {
            tile_script tile_script_ = t.GetComponent<tile_script>();
            //top
            if(tile_script_.tile_Y == (tile_Y + 1) && tile_script_.tile_X == tile_X) 
            {
                tile_script_.returnTile_number(false);
                
            }
            //topleft
            if(tile_script_.tile_Y == (tile_Y + 1) && tile_script_.tile_X == (tile_X - 1)) 
            {
                tile_script_.returnTile_number(false);
                
            }
            //topright
            if(tile_script_.tile_Y == (tile_Y + 1) && tile_script_.tile_X == (tile_X + 1)) 
            {
                tile_script_.returnTile_number(false);
                
            }
        }
    }
    if(downVertical)
    {
        foreach(Transform t in GameObject.Find("/grid_Container/tile_Container").transform)
        {
            tile_script tile_script_ = t.GetComponent<tile_script>();
            if(tile_script_.tile_Y == (tile_Y - 1) && tile_script_.tile_X == tile_X) 
            {
                tile_script_.returnTile_number(false);
            }
            //bottomleft
             if(tile_script_.tile_Y == (tile_Y - 1) && tile_script_.tile_X == (tile_X -1)) 
            {
                tile_script_.returnTile_number(false);
            }
            //bottomright
             if(tile_script_.tile_Y == (tile_Y - 1) && tile_script_.tile_X == (tile_X +1)) 
            {
                tile_script_.returnTile_number(false);
            }
        }
    }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
